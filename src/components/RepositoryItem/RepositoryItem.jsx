import React from "react";
import PropTypes from "prop-types";
import styles from './RepositoryItem.module.scss'

export const RepositoryItem = ({url, name, language, description}) => {
    return (
        <div className={styles.container}>
            <a href={url} target="_blank" rel="noreferrer noopener">
                <div className={styles.subContainer}>
                    <h3 id="repoName">{name}</h3>
                    <div>Language: {language}</div>
                    <div>Description: {description}</div>
                </div>
            </a>
        </div>
    )
}
RepositoryItem.propTypes = {
    url: PropTypes.any,
    name: PropTypes.string,
    language: PropTypes.string,
    description: PropTypes.string,
};

RepositoryItem.defaultProps = {
    url: null,
    name: '',
    language: '',
    description: '',
};
