import {createAction} from 'redux-actions';
import agent from '../../agent';

export const getAllRepositories = createAction('ALL/CHARACTER', async (termString, limit, offset) => {
    return await agent.Repositories.all(termString, limit, offset);
});


