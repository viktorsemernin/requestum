import {ActionType} from "redux-promise-middleware";
import {handleActions} from "redux-actions";
import {getAllRepositories} from "../action/repositories";

const reducer = handleActions(
    {
        [getAllRepositories.toString()]: {
            [ActionType.Fulfilled]: (state, action) => ({
                ...state,
                result: action.payload.items,
            }),
            [ActionType.Rejected]: (state, action) => ({
                ...state,
                ...action.payload,
            }),
        },

    },
    {
        result: []
    }
);

export default reducer;