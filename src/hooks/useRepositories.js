import {useSelector} from 'react-redux';
import useAsyncDispatch from './useAsyncDispatch';
import {getAllRepositories} from "../store/action/repositories";

export default function useRepositories() {
    const repositories = useSelector((state) => state.repositories);
    const dispatch = useAsyncDispatch();

    return {
        ...repositories,
        getAllRepositories: (termString, limit, offset) => dispatch(getAllRepositories(termString, limit, offset)),
    };
}
