import React from 'react';
import {Provider as StoreProvider} from 'react-redux';
import {Switch, Route, BrowserRouter} from 'react-router-dom';
import store from './store';
import Header from "./components/Header";
import Home from "./containers/Home";

const App = () => {

    return (
        <BrowserRouter>
            <Header/>
            <Switch>
                <Route exact path="/" component={Home}/>
            </Switch>
        </BrowserRouter>
    );
};

const AppProviders = () => (
    <StoreProvider store={store}>
        <App/>
    </StoreProvider>
);

export default AppProviders;
